/**
 * Constantes de las api's usadas
 */
const APP_SERVER = "http:192.169.181.36:9292";

export const API_BRANCHES = `${APP_SERVER}/WEB/API/Branches`;
export const API_TICKETS = `${APP_SERVER}/WEB/API/Tickets`;
export const API_SERVICES = `${APP_SERVER}/WEB/API/Services`;
export const API_LOGIN = `${APP_SERVER}/WEB/API/LogIn`;
