import { AsyncStorage } from 'react-native';

export const GOOGLE_KEY = 'AIzaSyAtI0SA90Mq-ZuepB599CG3CuP4WnNg_iQ';
export const USER_KEY = 'user-id-key';
export const USER_EMAIL = 'user-email-key';

export const onSignIn = userId => AsyncStorage.setItem(USER_KEY, JSON.stringify(userId));

export const onSignOut = () => {
  AsyncStorage.removeItem(USER_KEY);
  AsyncStorage.removeItem(USER_EMAIL);
};

export const isSignedIn = () => new Promise((resolve, reject) => {
  AsyncStorage.getItem(USER_KEY).then((userId) => {
    if (userId !== undefined) {
      resolve(userId);
    } else {
      const reason = new Error('User not found');
      reject(reason);
    }
  });
});
