import EStyleSheet from 'react-native-extended-stylesheet';

export const commonStyles = EStyleSheet.create({
  button: {
    position: 'absolute',
    top: 20,
    padding: 10,
  },
  caption: {
    fontSize: 20,
    fontWeight: 'bold',
    alignItems: 'center',
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  cellSeparator: {
    height: EStyleSheet.hairlineWidth,
    backgroundColor: '#EFEFEF',
    marginLeft: 0,
    alignSelf: 'stretch',
  },
  tabBarIcon: {
    width: 26,
    height: 26,
  },
});

export const Colors = {
  navigationTintColor: '#1c1c1c',
  brandColor: '#01B0EF',
  destroyColor: '#e53e00',
  lightActionButton: '#dde3ea',
  lightActionButtonTitle: '#444a51',
};
