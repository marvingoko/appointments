const TicketStatus = {
  open: 0,
  canceled: 2,
  resolved: 5,
  canceledByAgency: 6,
};

export default TicketStatus;
