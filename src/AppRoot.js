import React from "react";
import { Font, Icon } from "expo";
import { View } from "react-native";
import EStyleSheet from "react-native-extended-stylesheet";
import { createRootNavigator } from "./config/router";
import { isSignedIn } from "../utils/auth";

EStyleSheet.build({});
export default class AppRoot extends React.Component {
  state = {
    checkedSignIn: undefined,
    user: undefined
  };
  constructor(props) {
    super(props);
  }

  async componentWillMount() {
    await Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
      //...Icon.Ionicons.font,
      //...Icon.Entypo.font
    });

    isSignedIn()
      .then(user => this.setState({ checkedSignIn: true, user }))
      .catch(() => this.setState({ checkedSignIn: false }));
  }

  render() {
    const { checkedSignIn, user } = this.state;

    if (checkedSignIn === undefined) {
      return <View />;
    }

    const Layout = createRootNavigator(user);
    return <Layout screenProps={user} />;
  }
}
