import React from "react";
//import { Platform, StatusBar } from "react-native";
import {
  createStackNavigator,
  createBottomTabNavigator
} from "react-navigation";
import BranchesScreen from "../screens/branches";
import SupportScreen from "../screens/support";
import SignIn from "../screens/SignIn";
import Profile from "../screens/profile";
import CreateTicketNavigator from "../navigators/CreateTicketNavigator";
import HomeScreen from "../screens/Home";
import AppointmentsAvailabilityScreen from "../screens/AppointmentsAvailability";
import ConfirmOperationScreen from "../screens/ConfirmOperation";
import TabBarIcon from "../components/tabBarIcon";

const HomeNavigator = createStackNavigator(
  {
    HomeChild: {
      screen: HomeScreen
    },
    Reschedule: {
      screen: AppointmentsAvailabilityScreen
    },
    ConfirmOperation: {
      screen: ConfirmOperationScreen
    }
  },
  {
    headerMode: "screen"
  }
);

const MainModalNavigator = createStackNavigator(
  {
    HomeModal: HomeNavigator,
    CreateAppointmentModal: CreateTicketNavigator
  },
  {
    mode: "modal",
    headerMode: "none",
    cardStyle: {
      //paddingTop: Platform.OS === "ios" ? 0 : StatusBar.currentHeight
    }
  }
);

const SignedIn = createBottomTabNavigator(
  {
    Main: {
      screen: MainModalNavigator,
      navigationOptions: {
        tabBarLabel: "Home",
        tabBarIcon: nvProps => (
          <TabBarIcon
            icon={require("../../assets/home.png")}
            tintColor={nvProps.tintColor}
          />
        )
      }
    },
    Locations: {
      screen: BranchesScreen
    },
    Support: {
      screen: SupportScreen
    },
    Profile: {
      screen: Profile
    }
  },
  {
    animationEnabled: false,
    tabBarOptions: {
      inactiveTintColor: "grey",
      activeTintColor: "black",
      upperCaseLabel: false,
      showIcon: true,
      style: {
        height: 60,
        backgroundColor: "white"
      },
      indicatorStyle: {
        backgroundColor: "white"
      }
    }
  }
);

export const SignedOut = createStackNavigator(
  {
    SignIn: {
      screen: SignIn,
      navigationOptions: {
        title: "Sign In"
      }
    }
  },
  {
    headerMode: "none"
  }
);

export const createRootNavigator = (user = false) =>
  createStackNavigator(
    {
      SignedIn: {
        screen: SignedIn,
        navigationOptions: {
          gesturesEnabled: false
        }
      },
      SignedOut: {
        screen: SignedOut,
        navigationOptions: {
          gesturesEnabled: false
        }
      }
    },
    {
      headerMode: "none",
      mode: "modal",
      initialRouteName: user ? "SignedIn" : "SignedOut"
    }
  );
