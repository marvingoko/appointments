import React from "react";
import { createStackNavigator } from "react-navigation";
import PropTypes from "prop-types";
import SelectServiceScreen from "../../screens/SelectService";
import AppointmentsAvailabilityScreen from "../../screens/AppointmentsAvailability";
import ConfirmOperationScreen from "../../screens/ConfirmOperation";

const ProcessServiceStackNavigator = createStackNavigator({
  SelectService: {
    screen: SelectServiceScreen
  },
  AppointmentsAvailability: {
    screen: AppointmentsAvailabilityScreen
  },
  ConfirmOperation: ConfirmOperationScreen
});

class CreateTicketNavigator extends React.Component {
  static propTypes = {
    navigation: PropTypes.object
  };

  static defaultProps = {
    navigation: {}
  };

  render() {
    return (
      <ProcessServiceStackNavigator
        screenProps={{ rootNavigation: this.props.navigation }}
      />
    );
  }
}

export default CreateTicketNavigator;
