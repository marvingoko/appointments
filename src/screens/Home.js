import React from "react";
import {
  View,
  Alert,
  AsyncStorage,
  Text as RNText,
  StatusBar
} from "react-native";
import { Container, Content, Button, Text } from "native-base";
import AppLink from "react-native-app-link";
import PropTypes from "prop-types";
import moment from "moment/src/moment";
import { EventRegister } from "react-native-event-listeners";
import sectionListGetItemLayout from "react-native-section-list-get-item-layout";
import { Colors } from "../common/styles";
import { BrandHeader } from "../components/BrandHeader";
import TicketStatus from "../common/Enums";
import { USER_KEY } from "../../utils/auth";
import { TicketView } from "../components/TicketView";
import { API_TICKETS } from "../../utils/web_apis";

class HomeScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    headerTintColor: Colors.navigationTintColor,
    headerTitleStyle: { fontSize: 16, fontWeight: "normal" },
    headerTitle: () => <BrandHeader />,
    tabBarOnPress: ({ scene, jumpToIndex }) => {
      navigation.state.params.loadData();
      // Keep original behaviour
      jumpToIndex(scene.index);
    }
  });

  static propTypes = {
    navigation: PropTypes.object
  };

  static defaultProps = {
    navigation: {}
  };

  static onShowOnGoogleMaps(ticket) {
    const url = `https://www.google.com/maps/search/?api=1&query=${
      ticket.latitudeBranch
    },${ticket.longitudeBranch}`;
    AppLink.maybeOpenURL(url, {
      appName: "Google Maps",
      appStoreId: "id585027354",
      playStoreId: ""
    })
      .then(() => {
        // do stuff
      })
      .catch(() => {
        Alert.alert("Error redireccionando  a Google Maps");
      });
  }

  static cancelTicket(ticket) {
    fetch(`${API_TICKETS}?IdTicket=${ticket.id}`, {
      method: "DELETE"
    })
      .then(() => {
        EventRegister.emit("ticket_created", null);
      })
      .catch(() => {
        Alert.alert("Error: cancelar Ticket");
      });
  }

  static onCancelTicketTapped(ticket) {
    Alert.alert(
      "Cancelar?",
      "Está seguro que desea cancelar tu Ticket?",
      [
        {
          text: "Cancelar Ticket",
          onPress: () => {
            HomeScreen.cancelTicket(ticket);
          },
          style: "destructive"
        },
        { text: "Volver", onPress: () => {}, style: "cancel" }
      ],
      { cancelable: false }
    );
  }

  constructor(props) {
    super(props);
    this.state = { tickets: [], rootNavigation: props.navigation };
    this.getItemLayout = sectionListGetItemLayout({
      getItemHeight: () => 200
    });
  }

  componentDidMount() {
    const { navigation } = this.props;

    navigation.setParams({
      loadData: this.getUserData.bind(this)
    });

    this.listener = EventRegister.addEventListener("ticket_created", () =>
      this.getUserData()
    );
    this.getUserData();
    setInterval(this.getUserData.bind(this), 10000);
  }

  onNewRequirementTapped() {
    this.props.navigation.navigate("CreateAppointmentModal");
  }

  onRescheduleTicketTapped(ticket) {
    const serviceParams = {
      isRescheduling: true,
      oldAppointment: ticket
    };
    this.props.navigation.navigate("Reschedule", {
      serviceParams
    });
  }

  async getUserData() {
    AsyncStorage.getItem(USER_KEY).then(userId => {
      fetch(`${API_TICKETS}?idUser=${userId}`)
        .then(response => response.json())
        .then(responseJson => {
          const tickets = Array.isArray(responseJson) ? responseJson : [];
          const filteredTickets = tickets.filter(
            t =>
              t.status !== TicketStatus.canceled &&
              t.status !== TicketStatus.canceledByAgency
          );

          this.setState({ tickets: filteredTickets });
        })
        .catch(() => {
          // TODO: Handle this (show empty view)
        });
    });
  }

  renderTicketView() {
    if (this.state.tickets.length > 0) {
      const ticket = this.state.tickets[0];
      const date = moment(ticket.scheduledDate, "DD/MM/YYYY");
      const time = moment(ticket.scheduledTime, "HH:mm:ss");
      date.hour(time.hour());
      date.minute(time.minute());
      const appointment = {
        name: ticket.branch,
        date,
        waitTime: ticket.waitTime
      };

      return (
        <View>
          <TicketView
            showWaitTime
            ticketId={ticket.id}
            internalControl={ticket.internalControl}
            appointment={appointment}
            serviceName={ticket.service}
            categoryName=""
          />
          <View
            style={{
              flexDirection: "row",
              flex: 1,
              alignSelf: "stretch",
              marginHorizontal: 64,
              justifyContent: "center"
            }}
          >
            <RNText
              style={{ margin: 8, fontSize: 14, color: "green" }}
              onPress={() => {
                HomeScreen.onShowOnGoogleMaps(ticket);
              }}
            >
              {"Ruta"}
            </RNText>
            <RNText
              style={{ margin: 8, fontSize: 14, color: "gray" }}
              onPress={() => {
                this.onRescheduleTicketTapped(ticket);
              }}
            >
              {"Reprogramar"}
            </RNText>
            <RNText
              style={{ margin: 8, fontSize: 14, color: "gray" }}
              onPress={() => {
                HomeScreen.onCancelTicketTapped(ticket);
              }}
            >
              {"Cancelar"}
            </RNText>
          </View>
        </View>
      );
    }
    return (
      <View
        style={{
          height: 500,
          alignSelf: "stretch",
          alignItems: "center",
          justifyContent: "center"
        }}
      >
        <Text> {"Actualmente no existen tickets activos"} </Text>
      </View>
    );
  }

  render() {
    return (
      <Container>
        <StatusBar barStyle="dark-content" />
        <Content scrollEnabled>{this.renderTicketView()}</Content>
        <Button
          block
          style={{ margin: 8, backgroundColor: Colors.brandColor }}
          onPress={() => this.onNewRequirementTapped()}
        >
          <Text>Tomar Ticket</Text>
        </Button>
      </Container>
    );
  }
}

export default HomeScreen;
