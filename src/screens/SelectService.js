import React, { Component } from "react";
import PropTypes from "prop-types";
import { Button } from "native-base";
import { View, FlatList, Platform } from "react-native";
import { SingleLabelCell } from "../components/SingleLabelCell";
import { commonStyles, Colors } from "../common/styles";
import { Loader } from "../components/Loader";
import { API_SERVICES } from "../../utils/web_apis";
import Search from "../components/search";
import { Ionicons, MaterialIcons } from "@expo/vector-icons";
import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
  screenContainer: {
    backgroundColor: 'white',
    flex: 1,
    alignSelf: 'stretch',
  },
  sectionList: {
    backgroundColor: 'white',
    flex: 1,
    alignSelf: 'stretch',
  }
});

class SelectServiceScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;

    let currentNavigation = navigation;
    let image = Platform.OS === "ios" ? "ios-arrow-back" : "arrow-back";
    if (
      "serviceParams" in params === false ||
      params.serviceParams.typeId === undefined
    ) {
      image = Platform.OS === "ios" ? "ios-close" : "close";
      currentNavigation = params.rootNavigation;
    }
    return {
      headerStyle: {
        backgroundColor: "white"
      },
      headerLeft: (
        <Button
          transparent
          onPress={() => {
            params.handleClose(currentNavigation);
          }}
        >
          {Platform.OS === "ios" ? (
            <Ionicons
              name={image}
              size={30}
              color={Colors.navigationTintColor}
            />
          ) : (
            <MaterialIcons
              name={image}
              size={30}
              colcor={Colors.navigationTintColor}
            />
          )}
        </Button>
      ),
      headerTintColor: Colors.navigationTintColor,
      title: "Seleccione Requerimiento",
      headerTitleStyle: { fontSize: 16, fontWeight: "normal" }
    };
  };

  static propTypes = {
    navigation: PropTypes.object,
    screenProps: PropTypes.object
  };

  static defaultProps = {
    navigation: {},
    screenProps: {}
  };

  static onCloseButtonTapped(navigation) {
    const nParams = navigation.state.params;
    if (
      nParams !== undefined &&
      ("serviceParams" in nParams === true ||
        nParams.serviceParams.typeId !== undefined)
    ) {
      if (nParams.serviceParams.categoryId !== undefined) {
        delete nParams.serviceParams.categoryId;
        delete nParams.serviceParams.categoryName;
        navigation.navigate("SelectService", nParams);
      } else {
        delete nParams.homeKey;
        delete nParams.serviceParams;
        navigation.navigate("SelectService", nParams);
      }
    } else {
      navigation.goBack();
    }
  }

  static formatServiceTitle(title) {
    return SelectServiceScreen.replaceAt(
      title.toLowerCase(),
      0,
      title.charAt(0).toUpperCase()
    );
  }

  static replaceAt(string, index, replacement) {
    return (
      string.substr(0, index) +
      replacement +
      string.substr(index + replacement.length)
    );
  }

  constructor(props) {
    super(props);
    this.state = {
      searchTerm: "",
      services: undefined
    };
  }

  componentDidMount() {
    const { navigation } = this.props;
    const { rootNavigation } = this.props.screenProps;
    navigation.setParams({
      handleClose: SelectServiceScreen.onCloseButtonTapped,
      rootNavigation
    });

    if (navigation.getParam("services", undefined) === undefined) {
      this.getServices();
    }
  }

  onTextChanged(text) {
    this.setState({ searchTerm: text });
  }

  onItemSelected(item) {
    const { navigation } = this.props;
    const nParams = this.props.navigation.state.params;
    if (
      "serviceParams" in nParams === false ||
      nParams.serviceParams.typeId === undefined
    ) {
      Object.assign(nParams, {
        homeKey: navigation.state.key,
        serviceParams: {
          typeId: item.item.id
        }
      });
      navigation.navigate("SelectService", nParams);
    } else if (nParams.serviceParams.categoryId === undefined) {
      Object.assign(nParams.serviceParams, {
        categoryId: item.item.id,
        categoryName: item.item.name
      });
      navigation.navigate("SelectService", nParams);
    } else {
      Object.assign(nParams.serviceParams, {
        serviceId: item.item.id,
        serviceName: item.item.name
      });
      navigation.navigate("AppointmentsAvailability", nParams);
    }
  }

  getServices() {
    fetch(API_SERVICES, {
      method: "GET"
    })
      .then(response => response.json())
      .then(responseJson => {
        this.setState({ services: responseJson });
      });
  }

  services() {
    if (this.state.services) {
      return this.state.services;
    } else if (this.props.navigation.getParam("serviceParams")) {
      return this.props.navigation.state.params.serviceParams.services;
    }

    return undefined;
  }

  listItems() {
    const items = [];

    const nParams = this.props.navigation.state.params;

    // no services have been loaded
    if (this.services() === undefined) {
      return items;
    }

    if (
      nParams === undefined ||
      "serviceParams" in nParams === false ||
      "typeId" in nParams.serviceParams === false
    ) {
      return this.services().map(obj => ({
        name: obj.type[0].name,
        id: obj.type[0].id
      }));
    }
    const typeObj = this.services().filter(
      obj => obj.type[0].id === nParams.serviceParams.typeId
    )[0];

    if (nParams.serviceParams.categoryId === undefined) {
      return typeObj.type[0].categories.map(obj => ({
        name: obj.name,
        id: obj.id
      }));
    }
    const categoryObj = typeObj.type[0].categories.filter(
      category => category.id === nParams.serviceParams.categoryId
    )[0];

    return categoryObj.services.map(obj => ({ name: obj.name, id: obj.id }));
  }

  filteredListItems() {
    const { searchTerm } = this.state;
    var items = this.listItems();

    if (searchTerm.length > 0) {
      items = items.filter(item =>
        item.name.toUpperCase().includes(searchTerm.toUpperCase())
      );
    }
    return items;
  }

  render() {
    return (
      <View style={styles.screenContainer}>
        <Search onTextChanged={text => this.onTextChanged(text)} />
        <FlatList
          style={styles.sectionList}
          data={this.filteredListItems()}
          extraData={this.state.searchTerm}
          ItemSeparatorComponent={() => (
            <View style={commonStyles.cellSeparator} />
          )}
          keyExtractor={item => item.id}
          renderItem={item => (
            <SingleLabelCell
              text={SelectServiceScreen.formatServiceTitle(item.item.name)}
              item={item}
              onSelected={selectedItem => this.onItemSelected(selectedItem)}
            />
          )}
          ListEmptyComponent={<Loader height={300} />}
        />
      </View>
    );
  }
}

export default SelectServiceScreen;
