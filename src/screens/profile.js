import React, { Component } from "react";
import {
  View,
  AsyncStorage,
  StyleSheet,
  Platform,
  StatusBar
} from "react-native";
import { Container, Card, Content, Button, Text } from "native-base";
import PropTypes from "prop-types";
import { Colors, commonStyles } from "../common/styles";
import { BrandHeader } from "../components/BrandHeader";
import { onSignOut, USER_EMAIL } from "../../utils/auth";
import TabBarIcon from "../components/tabBarIcon";

class Profile extends Component {
  static navigationOptions = {
    tabBarLabel: "Perfil",
    tabBarIcon: nvProps => (
      <TabBarIcon
        icon={require("../../assets/gender_neutral_user_filled.png")}
        tintColor={nvProps.tintColor}
      />
    )
  };

  state = { userEmail: undefined };

  static propTypes = {
    navigation: PropTypes.object
  };

  static defaultProps = {
    navigation: {}
  };

  constructor(props) {
    super(props);
    AsyncStorage.getItem(USER_EMAIL).then(email => {
      this.setState({ userEmail: email });
    });
  }

  OnButtonPress() {
    onSignOut();
    this.props.navigation.navigate("SignedOut");
  }

  renderUsername() {
    const { userEmail } = this.state;

    if (userEmail === undefined) {
      return undefined;
    }

    return (
      <View style={styles.userEmailContainer}>
        <Text style={styles.userEmailText}>{userEmail}</Text>
      </View>
    );
  }
  render() {
    return (
      <Container style={styles.container}>
        <Content>
          <StatusBar barStyle="dark-content" />
          <BrandHeader />
          <View>
            <Card style={{ paddingTop: 0, marginTop: 130 }}>
              {this.renderUsername()}
              <Button
                block
                style={styles.boton}
                onPress={() => {
                  this.OnButtonPress();
                }}
              >
                <Text>Cerrar Sesión</Text>
              </Button>
            </Card>
          </View>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingTop: Platform.OS === "ios" ? 0 : StatusBar.currentHeight
  },
  boton: {
    backgroundColor: Colors.brandColor
  },
  userEmailContainer: {
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "center",
    marginVertical: 20,
    height: 30
  },
  userEmailText: { color: "#333333", fontSize: 24 }
});

export default Profile;
