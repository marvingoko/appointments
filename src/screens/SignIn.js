import React, { Component } from 'react';
import { AsyncStorage, Alert } from 'react-native';
import { PropTypes } from 'prop-types';
import {
  Input,
  Container,
  Content,
  Button,
  Text,
  Form,
  Item,
  Label,
} from 'native-base';
import { Colors } from '../common/styles';
import { onSignIn, USER_EMAIL } from '../../utils/auth';
import { Loader } from '../components/Loader';
import { API_LOGIN } from '../../utils/web_apis';
import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
  containerStyle: {
    marginTop: 150,
    borderWidth: 1,
    borderRadius: 2,
    borderColor: '#ddd',
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.1,
    shadowRadius: 2,
    elevation: 1,
    marginLeft: 5,
    marginRight: 5,
    backgroundColor: '#fff',
  },
  errorTextStyle: {
    fontSize: 16,
    flexDirection: 'row',
    alignSelf: 'center',
    color: 'red',
  },
});

class SignIn extends Component {
  static propTypes = {
    "navigation": PropTypes.Object,
  };

  static defaultProps = {
    "navigation": undefined,
  };

  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      error: '',
      loading: false,
    };
  }


  onButtonPress() {
    const { email, password } = this.state;

    this.setState({ error: '', loading: true });

    fetch(`${API_LOGIN}?Email=${email}&Password=${password}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
    }).then((response) => {
      if (response.status === 200) {
        response.json().then((json) => {
          this.onLoginSuccess(json.userId);
        });
      } else {
        this.onLoginFail();
      }
    })
      .catch(() => {
        this.onLoginFail();
      });
  }

  onLoginSuccess(userId) {
    const { email } = this.state;

    AsyncStorage.setItem(USER_EMAIL, email).then(() => {
      onSignIn(userId).then(() => {
        this.props.navigation.navigate('SignedIn', {
          user: email,
        });
      }).catch(() => {
        Alert.alert('Error de logeo');
      });
    });
  }

  onLoginFail() {
    this.setState({
      error: 'Usuario o contrasena incorrecta',
      loading: false,
    });
  }

  renderButton() {
    if (this.state.loading) {
      return <Loader color={Colors.brandColor} />;
    }
    return (
      <Button
        block
        style={{ backgroundColor: Colors.brandColor }}
        onPress={() => {
          this.onButtonPress();
        }}
      >
        <Text>Iniciar Sesión</Text>
      </Button>
    );
  }

  render() {
    return (
      <Container>
        <Content>
          <Form style={styles.containerStyle}>
            <Item stackedLabel>
              <Label>Nombre de Usuario</Label>
              <Input
                value={this.state.email}
                autoCorrect={false}
                onChangeText={email => this.setState({ email })}
              />
            </Item>
            <Item stackedLabel>
              <Label>Contraseña</Label>
              <Input
                secureTextEntry
                autoCorrect={false}
                value={this.state.password}
                onChangeText={password => this.setState({ password })}
              />
            </Item>
            <Item>
              <Text style={styles.errorTextStyle}>
                {this.state.error}
              </Text>
            </Item>

            {this.renderButton()}

          </Form>
        </Content>
      </Container>
    );
  }
}

export default SignIn;
