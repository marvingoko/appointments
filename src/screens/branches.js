import React, { Component } from "react";
import {
  View,
  Image,
  Text,
  Platform,
  StatusBar,
  StyleSheet
} from "react-native";
import TabBarIcon from "../components/tabBarIcon";

class BranchesScreen extends Component {
  static navigationOptions = {
    tabBarLabel: "Agencias",
    tabBarIcon: nvProps => (
      <TabBarIcon
        icon={require("../../assets/location_filled.png")}
        tintColor={nvProps.tintColor}
      />
    )
  };

  render() {
    return (
      <View style={styles.container}>
        <StatusBar barStyle="dark-content" />
        <Image source={require("../../assets/location_filled.png")} />
        <Text>Agencias</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    flex: 1,
    justifyContent: "center",
    paddingTop: Platform.OS === "ios" ? 0 : StatusBar.currentHeight
  }
});

export default BranchesScreen;
