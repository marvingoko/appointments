import React, { Component } from "react";
import {
  View,
  Image,
  Text,
  StyleSheet,
  Platform,
  StatusBar
} from "react-native";
import TabBarIcon from "../components/tabBarIcon";

class SupportScreen extends Component {
  static navigationOptions = {
    tabBarLabel: "Soporte",
    tabBarIcon: nvProps => (
      <TabBarIcon
        icon={require("../../assets/assistant_filled.png")}
        tintColor={nvProps.tintColor}
      />
    )
  };

  render() {
    return (
      <View style={styles.container}>
        <StatusBar barStyle="dark-content" />
        <Image source={require("../../assets/assistant_filled.png")} />
        <Text>Soporte</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    flex: 1,
    justifyContent: "center",
    paddingTop: Platform.OS === "ios" ? 0 : StatusBar.currentHeight
  }
});

export default SupportScreen;
