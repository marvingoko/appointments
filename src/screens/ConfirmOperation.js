import React from 'react';
import { View, AsyncStorage, Alert, Text as RNText, Platform } from 'react-native';
import AppLink from 'react-native-app-link';
import { PropTypes } from 'prop-types';
import { Button, Text, Container, Content } from 'native-base';
import { EventRegister } from 'react-native-event-listeners';
import { USER_KEY } from "../../utils/auth";
import { Colors } from "../common/styles";
import { Loader } from "../components/Loader";
import { TicketView } from "../components/TicketView";
import { API_TICKETS } from "../../utils/web_apis";
import { Ionicons, MaterialIcons } from "@expo/vector-icons";
import EStyleSheet from "react-native-extended-stylesheet";

const styles = EStyleSheet.create({
  content: {
    alignSelf: "stretch",
    flex: 1,
    backgroundColor: "whitesmoke",
    alignItems: "flex-start",
    justifyContent: "flex-end",
    paddingTop: 20
  }
});

class ConfirmOperationScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    const { rootNavigation } = params;
    return {
      headerStyle: {
        backgroundColor: 'white',
      },
      headerRight: params.showRightBarButton ? (
        <Button
          transparent
          onPress={() => {
            if (params.serviceParams.isRescheduling) {
              navigation.popToTop();
            } else {
              rootNavigation.goBack();
            }
          }}
        >
          <Text style={{ color: Colors.navigationTintColor }}>Listo</Text>
        </Button>
      ) : null,
      headerLeft: params.showRightBarButton ? null : (
        <Button transparent onPress={() => navigation.goBack()}>
        {Platform.OS === 'ios' ? (
            <Ionicons
              name="ios-arrow-back"
              size={30}
              color={Colors.navigationTintColor}
            />
          ) : (
            <MaterialIcons
              name="arrow-back"
              size={30}
              colcor={Colors.navigationTintColor}
            />
          )}
        </Button>
      ),
      headerTintColor: Colors.navigationTintColor,
      title: params.serviceParams.isRescheduling ? 'Reprogramar Ticket' : 'Crear Ticket',
      headerTitleStyle: { fontSize: 16, fontWeight: 'normal' },
    };
  };

  static onShowOnGoogleMaps(ticket) {
    const url = `https://www.google.com/maps/search/?api=1&query=${ticket.latitude},${
      ticket.longitude
    }`;
    AppLink.maybeOpenURL(url, {
      appName: 'Google Maps',
      appStoreId: 'id585027354',
      playStoreId: '',
    })
      .then(() => {
        // do stuff
      })
      .catch(() => {
        Alert.alert('Error redireccionando  a Google Maps');
      });
  }

  static propTypes = {
    navigation: PropTypes.object,
  };

  static defaultProps = {
    navigation: {},
  };

  constructor(props) {
    super(props);
    this.state = {
      ticketId: undefined,
      isLoading: false,
      ticketCreated: false,
    };
  }

  componentDidMount() {
    this.props.navigation.setParams({
      showRightBarButton: false,
    });
  }

  onConfirmButtonTapped() {
    AsyncStorage.getItem(USER_KEY).then((userId) => {
      const { serviceParams } = this.props.navigation.state.params;

      const body = {
        idBranch: serviceParams.appointment.id,
        latitude: serviceParams.userLocation.latitude,
        longitude: serviceParams.userLocation.longitude,
        hour: serviceParams.appointment.date.format('HH:mm'),
        date: serviceParams.appointment.date.format('DD/MM/YYYY'),
        idUser: userId,
        time: serviceParams.appointment.duration.replace(/\D/g, ''),
        idTransport: 6,
        typeAttention: 0,
      };

      const { isRescheduling: isRes } = serviceParams;

      Object.assign(body, {
        idService: isRes ? serviceParams.oldAppointment.idService : serviceParams.serviceId,
        idTicket: isRes ? serviceParams.oldAppointment.id : 0,
      });

      this.setState({ isLoading: true });
      fetch(API_TICKETS, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(body),
      })
        .then((response) => {
          if (response.status === 401) {
            Alert.alert('Ud ya tiene un Ticket activo');
            throw new Error('Error!');
          } else {
            return response.json();
          }
        })
        .then((responseJson) => {
          const { id: ticketId, internalControl } = responseJson;
          this.setState({
            ticket: responseJson,
            isLoading: false,
            ticketId,
            internalControl,
            ticketCreated: true,
          });

          this.props.navigation.setParams({
            showRightBarButton: true,
          });

          EventRegister.emit('ticket_created', null);
        })
        .catch(() => {
          this.setState({
            isLoading: false,
          });
        });
    });
  }

  renderButton() {
    const { isRescheduling } = this.props.navigation.state.params.serviceParams;
    if (this.state.ticketCreated) {
      return (
        <View
          style={{
            marginHorizontal: 64,
            marginVertical: 8,
            alignSelf: 'flex-start',
            backgroundColor: 'transparent',
          }}
        >
          <Text style={{ color: 'gray', fontSize: 14 }}>
            {isRescheduling ? 'Su Ticket fue reprogramado' : 'Su Ticket fue creado'}
          </Text>
          <RNText
            style={{ marginVertical: 8, fontSize: 14, color: 'black' }}
            onPress={() => {
              ConfirmOperationScreen.onShowOnGoogleMaps(this.state.ticket);
            }}
          >
            {'Ver ruta'}
          </RNText>
        </View>
      );
    } else if (this.state.isLoading) {
      return (
        <Button
          block
          style={{
            marginHorizontal: 64,
            marginVertical: 16,
            backgroundColor: Colors.lightActionButton,
            fontWeight: 'bold',
          }}
          onPress={() => this.onConfirmButtonTapped()}
        >
          <Loader />
        </Button>
      );
    }
    return (
      <Button
        block
        style={{
          marginHorizontal: 64,
          marginVertical: 16,
          backgroundColor: Colors.lightActionButton,
        }}
        onPress={() => this.onConfirmButtonTapped()}
      >
        <Text style={{ color: Colors.lightActionButtonTitle, fontWeight: 'bold' }}>
          {isRescheduling ? 'Reprogramar' : 'Confirmar'}
        </Text>
      </Button>
    );
  }

  render() {
    const { serviceParams } = this.props.navigation.state.params;
    const {
      categoryName, serviceName, appointment, oldAppointment,
    } = serviceParams;
    const { ticketId, internalControl } = this.state;
    return (
      <Container style={{ backgroundColor: 'whitesmoke' }}>
        <Content
          scrollEnabled={false}
          contentContainerStyle={[{ flex: 1, alignSelf: 'stretch' }, styles.content]}
        >
          <TicketView
            ticketId={ticketId}
            internalControl={internalControl}
            appointment={appointment}
            categoryName={serviceParams.isRescheduling ? '' : categoryName}
            serviceName={serviceParams.isRescheduling ? oldAppointment.service : serviceName}
          />
          {this.renderButton()}
        </Content>
      </Container>
    );
  }
}

export default ConfirmOperationScreen;
