import React from "react";
import {
  View,
  FlatList,
  AsyncStorage,
  Alert,
  Modal,
  Dimensions,
  Platform,
  TimePickerAndroid,
  DatePickerAndroid
} from "react-native";
import { PropTypes } from "prop-types";
import moment from "moment/src/moment";
import { MapView } from "expo";
import { Button, Text, Container, Content } from "native-base";
import { USER_KEY, GOOGLE_KEY } from "../../utils/auth";
import OpenAppointmentCell from "../components/OpenAppointmentCell";
import { commonStyles, Colors } from "../common/styles";
import { Loader } from "../components/Loader";
import { DateTimePicker } from "../components/DateTimePicker";
import { API_BRANCHES } from "../../utils/web_apis";
import { Ionicons, MaterialIcons } from "@expo/vector-icons";
import EStyleSheet from "react-native-extended-stylesheet";

/* eslint-disable import/no-unresolved */
const branchIconSource = require("../../assets/ic-branch.png");
/* eslint-enable import/no-unresolved */

const { width: SCREEN_WIDTH } = Dimensions.get("window");

class AppointmentsAvailabilityScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;

    return {
      headerStyle: {
        backgroundColor: "white"
      },
      headerLeft: (
        <Button
          transparent
          onPress={() => {
            if (params.serviceParams.isRescheduling === undefined) {
              navigation.navigate("SelectService");
            } else {
              navigation.goBack();
            }
          }}
        >
          {Platform.OS === "ios" ? (
            <Ionicons
              name="ios-arrow-back"
              size={30}
              color={Colors.navigationTintColor}
            />
          ) : (
            <MaterialIcons
              name="arrow-back"
              size={30}
              colcor={Colors.navigationTintColor}
            />
          )}
        </Button>
      ),
      headerRight: (
        <Button
          disabled={
            params.headerRightDisabled != null
              ? params.headerRightDisabled
              : true
          }
          transparent
          onPress={() => {
            navigation.navigate("ConfirmOperation", params);
          }}
        >
          <Text
            style={
              params.headerRightDisabled != null && !params.headerRightDisabled
                ? { color: Colors.navigationTintColor }
                : { color: "lightgray" }
            }
          >
            Continuar
          </Text>
        </Button>
      ),
      headerTintColor: Colors.navigationTintColor,
      title: "Agencias",
      headerTitleStyle: { fontSize: 16, fontWeight: "normal" }
    };
  };

  static propTypes = {
    navigation: PropTypes.object
  };

  static defaultProps = {
    navigation: {}
  };

  constructor(props) {
    super(props);

    this.state = {
      appointments: [],
      datePickerVisible: false,
      dateWasModified: false,
      date: moment()
    };
  }

  componentDidMount() {
    const { navigation } = this.props;
    navigation.setParams({
      headerRightDisabled: true
    });

    navigator.geolocation.getCurrentPosition(
      position => {
        this.setState(
          {
            userLocation: {
              latitude: position.coords.latitude,
              longitude: position.coords.longitude
            }
          },
          () => {
            this.getAvailability();
          }
        );
      },
      () => null,
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
    );
  }

  onBranchSelectedAtIndex(index) {
    this.branchCarousel.scrollToIndex({ index, viewOffset: 0 });
  }

  async onEditDateTapped() {
    if (Platform.OS === "ios") {
      this.setDateTimeSelectionVisible(true);
    } else {
      this.openAndroidDatePicker();
    }
  }

  onDateUpdated(date) {
    const { selectedAppointment } = this.state;
    Object.assign(selectedAppointment, { date });
  }

  async onAppointmentSelected(appointment) {
    // wipe out old values first
    await this.setStateAsync({
      durationToBranch: undefined,
      distanceToBranch: undefined
    });
    const {
      latitude: branchLatitude,
      longitude: branchLongitude
    } = appointment;
    const {
      latitude: userLatitude,
      longitude: userLongitude
    } = this.state.userLocation;
    const url = `https://maps.googleapis.com/maps/api/directions/json?origin=${userLatitude},${userLongitude}&destination=${branchLatitude},${branchLongitude}&key=${GOOGLE_KEY}&language=es`;
    fetch(url)
      .then(response => response.json())
      .then(response => {
        const leg = response.routes[0].legs[0];
        const startCoor = {
          latitude: leg.start_location.lat,
          longitude: leg.start_location.lng
        };
        const endCoor = {
          latitude: leg.end_location.lat,
          longitude: leg.end_location.lng
        };
        const coordinates = [];
        coordinates.push(startCoor);

        leg.steps.forEach(
          ({ start_location: startLoc, end_location: endLoc }) => {
            coordinates.push({ latitude: startLoc.lat, longitude: endLoc.lng });
            coordinates.push({ latitude: startLoc.lat, longitude: endLoc.lng });
          }
        );

        coordinates.push(endCoor);
        this.setState({ routeToBranch: { coordinates } });
        this.mapView.fitToCoordinates([startCoor, endCoor], {
          edgePadding: {
            top: 40,
            right: 40,
            bottom: 40,
            left: 40
          },
          animated: true
        });
      })
      .catch(e => {
        //console.warn(e);
      });

    const disDur = await this.getDistanceMatrixForAppointment(appointment);
    Object.assign(appointment, disDur);
    await this.setStateAsync({
      selectedAppointment: appointment,
      distanceToBranch: disDur.distance,
      durationToBranch: disDur.duration
    });

    this.getDistanceMatrixForAppointment(appointment);
    this.updateNavigationParameters();
  }

  setStateAsync(state) {
    return new Promise(resolve => {
      this.setState(state, resolve);
    });
  }

  async getDistanceMatrixForAppointment(appointment) {
    const {
      latitude: branchLatitude,
      longitude: branchLongitude
    } = appointment;
    const {
      latitude: userLatitude,
      longitude: userLongitude
    } = this.state.userLocation;
    return fetch(
      `https://maps.googleapis.com/maps/api/distancematrix/json?origins=${userLatitude},${userLongitude}&destinations=${branchLatitude},${branchLongitude}&key=${GOOGLE_KEY}&language=es`
    )
      .then(response => response.json())
      .then(response => ({
        distance: response.rows[0].elements[0].distance.text,
        duration: response.rows[0].elements[0].duration.text
      }));
  }

  getAvailability() {
    AsyncStorage.getItem(USER_KEY).then(userId => {
      const { serviceParams } = this.props.navigation.state.params;

      let serviceId;
      if (serviceParams.isRescheduling === true) {
        ({ idService: serviceId } = serviceParams.oldAppointment);
      } else {
        ({ serviceId } = serviceParams);
      }

      const params = {
        idService: serviceId,
        latitude: this.state.userLocation.latitude,
        longitude: this.state.userLocation.longitude,
        idUser: userId,
        typeAttention: 0
      };

      const encodedURL = Object.keys(params).reduce(
        (previous, key) => `${previous}&${key}=${params[key]}`,
        `${API_BRANCHES}?`
      );

      fetch(encodedURL)
        .then(response => response.json())
        .then(responseJson => {
          responseJson.forEach(appointment => {
            Object.assign(appointment, {
              date: moment()
            });
          });

          this.setState({ appointments: responseJson }, () => {
            const { appointments } = this.state;
            if (appointments.length > 0) {
              this.mapView.animateToRegion({
                latitude: appointments[0].latitude,
                longitude: appointments[0].longitude,
                latitudeDelta: 0.0322,
                longitudeDelta: 0.0321
              });

              this.onAppointmentSelected(appointments[0]);
            }
          });
        })
        .catch(e => {
          Alert.alert(JSON.stringify(e));
        });
    });
  }

  setDateTimeSelectionVisible(visible) {
    this.setState({ datePickerVisible: visible });
  }

  async openAndroidDatePicker() {
    try {
      const { action, year, month, day } = await DatePickerAndroid.open({
        date: new Date()
      });
      if (action !== DatePickerAndroid.dismissedAction) {
        const { date } = this.state;
        date.date(day);
        date.month(month);
        date.year(year);

        this.setState({
          dateWasModified: moment().diff(date, "minutes") >= 30
        });

        this.onDateUpdated(date);
        this.setState({ date }, () => {
          this.updateNavigationParameters();
        });
        this.openAndroidTimePicker();
      }
    } catch ({ code, message }) {
      //console.warn("Cannot open date picker", message);
    }

    const { time } = this.state;
    //console.log(time);
  }

  async openAndroidTimePicker() {
    try {
      const { action, hour, minute } = await TimePickerAndroid.open({
        hour: moment().hour(),
        minute: moment().minute(),
        is24Hour: false,
        mode: "spinner"
      });
      if (action !== TimePickerAndroid.dismissedAction) {
        this.setState({ dateWasModified: moment().hour() !== hour });

        const { date } = this.state;
        date.hour(hour);
        date.minute(minute);
        this.onDateUpdated(date);
        if (moment().diff(date, "minutes") >= 30) {
          this.setState({ dateWasModified: true });
        }

        this.setState({ date }, () => {
          this.updateNavigationParameters();
        });
      }
    } catch ({ code, message }) {
      //console.warn("Cannot open date picker", message);
    }
  }

  updateNavigationParameters() {
    const { navigation } = this.props;

    const { serviceParams } = navigation.state.params;
    const { selectedAppointment } = this.state;
    Object.assign(selectedAppointment, {
      date: this.state.date
    });
    Object.assign(serviceParams, {
      userLocation: {
        latitude: this.state.userLocation.latitude,
        longitude: this.state.userLocation.longitude
      },
      appointment: selectedAppointment
    });

    navigation.setParams({
      serviceParams,
      headerRightDisabled: false
    });
  }

  renderRoutetoBranch() {
    const { coordinates } = this.state.routeToBranch;
    return (
      <MapView.Polyline
        coordinates={coordinates}
        strokeColor="#1ba1e2"
        strokeWidth={2}
      />
    );
  }

  render() {
    return (
      <Container style={{ backgroundColor: "whitesmoke" }}>
        <Content
          scrollEnabled={false}
          contentContainerStyle={[
            { flex: 1, alignSelf: "stretch" },
            styles.content
          ]}
        >
          <MapView
            showsUserLocation
            style={{ flex: 1 }}
            ref={mapView => {
              this.mapView = mapView;
            }}
          >
            {this.state.routeToBranch === undefined
              ? undefined
              : this.renderRoutetoBranch()}
            {this.state.appointments.map((appointment, index) => (
              <MapView.Marker
                key={appointment.id}
                coordinate={{
                  latitude: appointment.latitude,
                  longitude: appointment.longitude
                }}
                onPress={() => {
                  this.onBranchSelectedAtIndex(index);
                }}
                image={branchIconSource}
                title={appointment.name}
              />
            ))}
          </MapView>
          <View style={styles.dateTimeSelectionContainer}>
            <MaterialIcons
              name="edit"
              style={{
                position: "absolute",
                right: 32,
                top: 32
              }}
              size={22}
              color="white"
              onPress={() => {
                this.onEditDateTapped();
              }}
            />
            <Text style={styles.timeText}>
              {this.state.date.format("DD/MM/YYYY")}
            </Text>
            <Text style={styles.dateText}>
              {this.state.date.format("hh:mm a")}
            </Text>
          </View>
          <View style={styles.branchesHorizontalListContainer}>
            <FlatList
              horizontal
              pagingEnabled
              ref={branchCarousel => {
                this.branchCarousel = branchCarousel;
              }}
              onMomentumScrollEnd={e => {
                const { contentOffset } = e.nativeEvent;
                const viewSize = e.nativeEvent.layoutMeasurement;
                const pageNum = Math.floor(contentOffset.x / viewSize.width);
                const appointment = this.state.appointments[pageNum];
                this.onAppointmentSelected(appointment);
              }}
              data={this.state.appointments}
              ItemSeparatorComponent={() => (
                <View style={commonStyles.cellSeparator} />
              )}
              getItemLayout={(data, index) => ({
                length: SCREEN_WIDTH,
                offset: SCREEN_WIDTH * index,
                index
              })}
              extraData={this.state.appointments}
              keyExtractor={item => item.id}
              renderItem={item => (
                <View style={{ width: SCREEN_WIDTH, flex: 1 }}>
                  <OpenAppointmentCell
                    shouldShowWaitTime={!this.state.dateWasModified}
                    appointment={Object.assign(item.item, {
                      distance: this.state.distanceToBranch,
                      duration: this.state.durationToBranch
                    })}
                    selected={item.item === this.state.selectedAppointment}
                    userLocation={this.state.userLocation}
                  />
                </View>
              )}
              ListEmptyComponent={<Loader height={200} />}
            />
          </View>
          <Modal
            animationType="slide"
            transparent={false}
            onRequestClose={() => {}}
            visible={this.state.datePickerVisible}
          >
            <DateTimePicker
              onCloseButtonTapped={() => {
                this.setDateTimeSelectionVisible(false);
              }}
              onDateTimeSelected={unformattedDate => {
                const date = moment(unformattedDate);
                const now = moment();
                const diff = Math.abs(now.diff(date, "minutes"));
                const dateWasModified = diff >= 30;
                this.setState({
                  date,
                  dateWasModified
                });
                this.onDateUpdated(date);
                this.setDateTimeSelectionVisible(false);
              }}
            />
          </Modal>
        </Content>
      </Container>
    );
  }
}

const styles = EStyleSheet.create({
  icon: {
    width: 24,
    height: 24
  },
  branchesHorizontalListContainer: {
    backgroundColor: "white",
    height: 80
  },
  dateTimeSelectionContainer: {
    backgroundColor: Colors.brandColor,
    height: 90,
    alignItems: "center",
    justifyContent: "center"
  },
  timeText: {
    fontSize: 36,
    alignSelf: "center",
    color: "white"
  },
  dateText: {
    fontSize: 16,
    alignSelf: "center",
    color: "white"
  },
  changeDateButton: {
    alignSelf: "center"
  }
});

export default AppointmentsAvailabilityScreen;
