import React from 'react';
import PropTypes from 'prop-types';
import {
  Platform,
  Text,
  View,
  Image,
  TouchableOpacity,
  TouchableNativeFeedback,
} from 'react-native';
import styles from './styles';
/* eslint-disable import/no-unresolved */
const calendarIconSource = require('../../../assets/calendar-icon.png');
/* eslint-enable import/no-unresolved */

class AppointmentCell extends React.Component {
  static propTypes = {
    ticket: PropTypes.object.isRequired,
    onSelected: PropTypes.func,
  };

  static defaultProps = {
    onSelected: () => {},
  }

  cellContent() {
    const { ticket } = this.props;
    return (
      <View style={styles.contentView}>
        <Image
          source={calendarIconSource}
          style={styles.icon}
        />
        <View>
          <Text style={styles.title}>{ticket.Servicio}</Text>
          <Text style={styles.subtitle}>
            {`${ticket.FechaProgramada} ${ticket.HoraProgramada}`}
          </Text>
        </View>
      </View>
    );
  }

  render() {
    const { onSelected, ticket } = this.props;
    return Platform.OS === 'ios' ? (
      <TouchableOpacity
        style={styles.cellContainer}
        onPress={() => onSelected(ticket)}
      >
        {this.cellContent()}
      </TouchableOpacity>
    ) : (
      <TouchableNativeFeedback
        style={styles.cellContainer}
        onPress={() => onSelected(ticket)}
      >
        {this.cellContent()}
      </TouchableNativeFeedback>
    );
  }
}

export default AppointmentCell;
