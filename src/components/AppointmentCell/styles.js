import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
  cellContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 10,
    backgroundColor: '#FFF',
  },
  contentView: {
    flex: 1,
    height: 80,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: '#FFF',
    alignSelf: 'stretch',
  },
  title: {
    color: '#000',
    fontSize: 18,
    paddingLeft: 8,
  },
  subtitle: {
    color: '#333',
    fontSize: 16,
    paddingLeft: 8,
  },
  icon: {
    height: 40,
    width: 40,
  },
});

export default styles;
