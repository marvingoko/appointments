import React from 'react';
import {
  Platform,
  Text,
  View,
  Image,
  TouchableOpacity,
  TouchableNativeFeedback,
} from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';


/* eslint-disable import/no-unresolved */
const ticketIconSource = require('../../../assets/ticket-icon.png');
/* eslint-enable import/no-unresolved */

class TicketCell extends React.Component {
  static propTypes = {
    ticket: PropTypes.object.isRequired,
    onSelected: PropTypes.func.isRequired,
  };

  cellContent() {
    const { ticket } = this.props;

    return (
      <View style={styles.contentView}>
        <Image
          source={ticketIconSource}
          style={styles.icon}
        />
        <View>
          <Text style={styles.title}>{ticket.service}</Text>
          <Text style={styles.subtitle}>
            {`${ticket.scheduledDate} ${ticket.scheduledTime}`}
          </Text>
          <Text style={styles.subtitle}>
            {`Tiempo de espera: ${ticket.waitTime} minutos`}
          </Text>
        </View>
      </View>
    );
  }

  render() {
    const { onSelected, ticket } = this.props;
    return Platform.OS === 'ios' ? (
      <TouchableOpacity
        style={styles.cellContainer}
        onPress={() => onSelected(ticket)}
      >
        {this.cellContent()}
      </TouchableOpacity>
    ) : (
      <TouchableNativeFeedback
        style={styles.cellContainer}
        onPress={() => onSelected(ticket)}
      >
        {this.cellContent()}
      </TouchableNativeFeedback>
    );
  }
}

export default TicketCell;
