import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
  title: {
    fontSize: 12,
    fontWeight: 'bold',
    alignSelf: 'stretch',
  },
  subtitle: {
    marginLeft: 16,
    fontSize: 13,
    fontWeight: 'normal',
    alignSelf: 'stretch',
    flexWrap: 'wrap',
    flex: 1,
    textAlign: 'right',
  },
  ticketDetailRow: {
    justifyContent: 'space-between',
    alignItems: 'center',
    alignSelf: 'stretch',
    flexDirection: 'row',
    marginHorizontal: 16,
    marginTop: 8,
    minHeight: 40,
  },
  ticketContainer: {
    alignSelf: 'stretch',
    flex: 1,
    marginHorizontal: 64,
    marginVertical: 16,
    backgroundColor: 'white',
    shadowColor: '#c0c0c0',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    borderRadius: 10,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
});

export default styles;
