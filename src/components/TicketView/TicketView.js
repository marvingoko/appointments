import React from 'react';
import { View, Text, Image } from 'react-native';
import PropTypes from 'prop-types';
import moment from 'moment/src/moment';
import QRCode from 'react-native-qrcode';
import styles from './styles';
import { commonStyles } from '../../common/styles';

/* eslint-disable import/no-unresolved */
const qrPlaceholderSource = require('../../../assets/qrcode_placeholder.png');
/* eslint-enable import/no-unresolved */

class TicketView extends React.Component {
  static propTypes = {
    ticketId: PropTypes.string,
    internalControl: PropTypes.string,
    appointment: PropTypes.object.isRequired,
    categoryName: PropTypes.string.isRequired,
    serviceName: PropTypes.string.isRequired,
    showWaitTime: PropTypes.bool,
  };

  static defaultProps = {
    ticketId: undefined,
    internalControl: undefined,
    showWaitTime: false,
  };

  renderQR() {
    const { ticketId, internalControl } = this.props;
    if (ticketId !== undefined) {
      return (
        <View style={{ alignItems: 'center', alignSelf: 'center', marginTop: 16 }}>
          <QRCode value={ticketId.toString()} size={100} bgColor="black" fgColor="white" />
          <Text style={{ fontSize: 22, fontWeight: 'bold' }}>{internalControl}</Text>
        </View>
      );
    }
    return <Image source={qrPlaceholderSource} style={{ width: 100, height: 100 }} />;
  }

  renderTicketDetails() {
    const { categoryName, serviceName, appointment } = this.props;
    const service = `${categoryName} ${serviceName}`;

    return (
      <View style={{ alignSelf: 'stretch', marginTop: 32 }}>
        <View style={styles.ticketDetailRow}>
          <Text style={styles.title}>Requerimiento</Text>
          <Text style={styles.subtitle} numberOfLines={4} ellipsizeMode="tail">
            {service}
          </Text>
        </View>
        <View style={commonStyles.cellSeparator} />
        <View style={styles.ticketDetailRow}>
          <Text style={styles.title}>Agencia</Text>
          <Text style={styles.subtitle}>{appointment.name}</Text>
        </View>
        <View style={commonStyles.cellSeparator} />
        <View style={styles.ticketDetailRow}>
          <Text style={styles.title}>Fecha</Text>
          <Text style={styles.subtitle}>{appointment.date.format('DD/MM/YYYY')}</Text>
        </View>
        <View style={commonStyles.cellSeparator} />
        <View style={styles.ticketDetailRow}>
          <Text style={styles.title}>Hora</Text>
          <Text style={styles.subtitle}>{appointment.date.format('hh:mm a')}</Text>
        </View>
        <View style={commonStyles.cellSeparator} />
        {Math.abs(moment().diff(appointment.date, 'minutes')) < 30 || this.props.showWaitTime ? (
          <View style={styles.ticketDetailRow}>
            <Text style={styles.title}>Tiempo de espera: </Text>
            <Text style={styles.subtitle}>{appointment.waitTime}</Text>
          </View>
        ) : (
          undefined
        )}
        {appointment.duration ? (
          <View style={styles.ticketDetailRow}>
            <Text style={styles.title}>Tiempo de traslado: </Text>
            <Text style={styles.subtitle}>
              {`${appointment.duration.toString().replace(/\D/g, '')} minutos`}
            </Text>
          </View>
        ) : (
          undefined
        )}
      </View>
    );
  }

  render() {
    return (
      <View style={styles.ticketContainer}>
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          {this.renderQR()}
        </View>
        {this.renderTicketDetails()}
      </View>
    );
  }
}

export default TicketView;
