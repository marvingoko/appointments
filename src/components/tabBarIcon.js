import React from "react";
import { Image } from "react-native";
import { commonStyles } from "../common/styles";

function TabBarIcon({icon, tintColor}) {
  return (
    <Image
      source={icon}
      style={[commonStyles.tabBarIcon, { tintColor }]}
    />
  );
}

export default TabBarIcon;
