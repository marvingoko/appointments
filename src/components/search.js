import React from "react";
import { StyleSheet, Platform } from "react-native";
import { Item, Input } from "native-base";
import { Ionicons, MaterialIcons } from '@expo/vector-icons';

function Search({ onTextChanged }) {
  return (
    <Item style={styles.searchBar}>
      {Platform.OS === "ios" ? 
      <Ionicons name="ios-search" size={27}/>:
      <MaterialIcons name="search" size={27}/>}
      <Input placeholder="Buscar" onChangeText={text => onTextChanged(text)} />
    </Item>
  );
}

const styles = StyleSheet.create({
  searchBar: {
    borderRadius: 20,
    backgroundColor: "whitesmoke",
    margin: 8,
    paddingHorizontal: 12,
    height: 44
  }
});

export default Search