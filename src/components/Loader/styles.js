import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
  loader: {
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default styles;
