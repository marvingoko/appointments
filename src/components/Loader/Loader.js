import React from 'react';
import { PropTypes } from 'prop-types';
import { ActivityIndicator } from 'react-native';
import styles from './styles';

const Loader = ({ height }) =>
  (
    <ActivityIndicator
      animating
      style={[styles.loader, { height }]}
    />
  );

Loader.propTypes = {
  height: PropTypes.number,
};
Loader.defaultProps = {
  height: 40,
};
export default Loader;
