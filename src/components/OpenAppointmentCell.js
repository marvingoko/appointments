import React from "react";
import PropTypes from "prop-types";
import moment from "moment/src/moment";
import { Text, View, Image } from "react-native";
import { Colors } from "../common/styles";
import EStyleSheet from "react-native-extended-stylesheet";

const styles = EStyleSheet.create({
  cellContainer: {
    height: 80,
    alignItems: "center",
    justifyContent: "center",
    paddingRight: 10,
    flexDirection: "row"
  },
  contentView: {
    height: 80,
    flex: 1,
    flexDirection: "row",
    padding: 10,
    alignItems: "center",
    justifyContent: "space-between",
    backgroundColor: "#FFF",
    borderRadius: 5
  },
  title: {
    flex: 1,
    fontSize: 18,
    justifyContent: "flex-start"
  },
  timeText: {
    flex: 1,
    fontSize: 12,
    marginRight: 8,
    textAlign: "right"
  },
  subtitle: {
    flex: 1,
    justifyContent: "flex-start",
    fontSize: 16,
    color: "grey"
  },
  distanceInformationText: {
    justifyContent: "flex-start",
    fontSize: 14,
    color: "gray"
  },
  text: {
    color: "#000"
  },
  icon: {
    height: 40,
    width: 40
  },
  carIcon: {
    alignSelf: "flex-start"
  },
  distanceEstimationContainer: {
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "flex-start",
    marginTop: 8,
    flex: 1
  },
  markerIcon: {
    height: 26,
    width: 26
  }
});

/* eslint-disable import/no-unresolved */
const carIconSource = require("../../assets/ic-car.png");
const markerIconSource = require("../../assets/location_filled.png");
/* eslint-enable import/no-unresolved */

class OpenAppointmentCell extends React.Component {
  static propTypes = {
    appointment: PropTypes.object.isRequired,
    selected: PropTypes.bool,
    shouldShowWaitTime: PropTypes.bool
  };

  static defaultProps = {
    selected: false,
    shouldShowWaitTime: false
  };

  callback(response) {
    this.console.log(JSON.stringify(response));
  }

  renderCellContent() {
    const { appointment } = this.props;
    return (
      <View style={styles.contentView}>
        <View
          style={{
            flexDirection: "column",
            alignItems: "flex-start",
            flex: 1
          }}
        >
          <View
            style={{ flexDirection: "row", justifyContent: "space-between" }}
          >
            <Text style={styles.title}>{appointment.name}</Text>
            <Text style={[styles.timeText, { marginRight: 16 }]}>
              {`${appointment.date.format(
                "DD/MM/YYYY"
              )} ${appointment.date.format("hh:mm a")}`}
            </Text>
          </View>
          {this.renderDistanceEstimation()}
          {this.props.shouldShowWaitTime ? this.renderWaitTime() : undefined}
        </View>
        <Image
          tintColor="red"
          source={markerIconSource}
          style={styles.markerIcon}
          resizeMode="center"
        />
      </View>
    );
  }

  renderDistanceEstimation() {
    const { appointment } = this.props;
    if (appointment.distance && appointment.duration) {
      return (
        <View style={styles.distanceEstimationContainer}>
          <Text style={styles.distanceInformationText}>
            {`Distancia: ${appointment.distance}, Traslado: ${
              appointment.duration
            }`}
          </Text>
          <Image
            source={carIconSource}
            style={styles.carIcon}
            resizeMode="center"
          />
        </View>
      );
    }
    return (
      <View style={styles.distanceEstimationContainer}>
        <Text style={styles.distanceInformationText}>
          Estimando distancia...
        </Text>
      </View>
    );
  }

  renderWaitTime() {
    return (
      <Text style={styles.distanceInformationText}>
        {this.props.appointment.waitTime
          ? `Tiempo de espera: ${this.props.appointment.waitTime} min`
          : `Tiempo de espera: No Disponible`}
      </Text>
    );
  }

  renderSelectionIndicator() {
    const { selected } = this.props;
    return selected ? (
      <View
        style={{
          backgroundColor: Colors.brandColor,
          width: 5,
          alignSelf: "stretch"
        }}
      />
    ) : (
      undefined
    );
  }

  render() {
    return (
      <View style={styles.cellContainer}>
        {this.renderSelectionIndicator()}
        {this.renderCellContent()}
      </View>
    );
  }
}

export default OpenAppointmentCell;
