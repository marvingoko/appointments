import React from "react";
import { View, Image, Platform } from "react-native";
import styles from "./styles";

/* eslint-disable import/no-unresolved */
const brandIconSource = require("../../../assets/brand-logo.png");
/* eslint-enable import/no-unresolved */
const BrandHeader = () => (
  <View
    style={[styles.header, Platform.OS === "ios" ? { paddingTop: 20 } : {}]}
  >
    <Image
      resizeMode="contain"
      style={[
        { width: 120 },
        Platform.OS === "ios" ? { marginBottom: 10 } : {}
      ]}
      source={brandIconSource}
    />
  </View>
);
export default BrandHeader;
