import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
  cellContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 10,
    backgroundColor: '#FFF',
  },
  contentView: {
    flex: 1,
    height: 45,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: '#FFF',
    alignSelf: 'stretch',
  },
  text: {
    fontSize: 16,
    color: '#000',
  },
});

export default styles;
