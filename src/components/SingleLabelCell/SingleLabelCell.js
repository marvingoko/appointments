import React from 'react';
import {
  Platform,
  Text,
  View,
  TouchableOpacity,
  TouchableNativeFeedback,
} from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';


class SingleLabelCell extends React.Component {
  static propTypes = {
    text: PropTypes.string.isRequired,
    item: PropTypes.object,
    onSelected: PropTypes.func.isRequired,
  };

  static defaultProps = {
    item: {},
  }

  cellContent() {
    return (
      <View style={styles.contentView}>
        <Text style={styles.text}> {this.props.text} </Text>
      </View>
    );
  }

  render() {
    const { item } = this.props;
    return Platform.OS === 'ios' ? (
      <TouchableOpacity
        style={styles.cellContainer}
        onPress={() => this.props.onSelected(item)}
      >
        {this.cellContent()}
      </TouchableOpacity>
    ) : (
      <TouchableNativeFeedback
        style={styles.cellContainer}
        onPress={() => this.props.onSelected(item)}
      >
        {this.cellContent()}
      </TouchableNativeFeedback>
    );
  }
}

export default SingleLabelCell;
