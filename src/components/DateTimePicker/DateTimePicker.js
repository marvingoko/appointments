import React from 'react';
import PropTypes from 'prop-types';
import { View, DatePickerIOS } from 'react-native';
import { Button, Icon, Text } from 'native-base';
import styles from './styles';

class DateTimePicker extends React.Component {
  static propTypes = {
    onCloseButtonTapped: PropTypes.func.isRequired,
    onDateTimeSelected: PropTypes.func.isRequired,
  };

  static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {
      selectedDate: new Date(),
    };

    this.setDate = this.setDate.bind(this);
  }

  setDate(newDate) {
    this.setState({ selectedDate: newDate });
  }

  render() {
    return (
      <View style={styles.container}>
        <Icon
          name="ios-close"
          style={styles.closeButton}
          onPress={() => {
            this.props.onCloseButtonTapped(this.state.selectedDate);
          }}
        />
        <View style={styles.pickerContainer}>
          <DatePickerIOS
            locale="es_ES"
            date={this.state.selectedDate}
            onDateChange={this.setDate}
          />
        </View>
        <Button
          block
          style={styles.continueButton}
          onPress={() => {
            this.props.onDateTimeSelected(this.state.selectedDate);
          }}
        >
          <Text
            style={{
              color: 'white',
              fontSize: 16,
            }}
          >
            Continuar
          </Text>
        </Button>
      </View>
    );
  }
}

export default DateTimePicker;
