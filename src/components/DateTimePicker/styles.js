import EStyleSheet from 'react-native-extended-stylesheet';
import { Colors } from '../../common/styles';

const styles = EStyleSheet.create({
  container: {
    flex: 1,
    alignSelf: 'stretch',
    marginTop: 40,
  },
  closeButton: {
    fontSize: 36,
    color: 'gray',
    marginLeft: 16,
    marginTop: 0,
    position: 'absolute',
  },
  continueButton: {
    position: 'absolute',
    left: 16,
    right: 16,
    bottom: 16,
    height: 50,
    backgroundColor: Colors.brandColor,
  },
  pickerContainer: {
    flex: 1,
    justifyContent: 'center',
    margin: 60,
  },
});

export default styles;
