import EStyleSheet from 'react-native-extended-stylesheet';
import { Colors } from '../../common/styles';

const styles = EStyleSheet.create({
  container: {
    flex: 1,
    height: 24,
    paddingLeft: 6,
    backgroundColor: Colors.brandColor,
    justifyContent: 'center',
  },
  text: {
    color: 'white',
  },
});

export default styles;
