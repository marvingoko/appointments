import React from 'react';
import PropTypes from 'prop-types';
import { Text, View } from 'react-native';
import styles from './styles';

const TitleSectionHeader = ({ text }) =>
  (
    <View style={styles.container}>
      <Text style={styles.text}> {text} </Text>
    </View>
  );

TitleSectionHeader.propTypes = {
  text: PropTypes.string.isRequired,
};

export default TitleSectionHeader;
